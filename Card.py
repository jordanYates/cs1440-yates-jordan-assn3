import sys

import NumberSet

class Card():
    myNumbers = []
    cardSize = 0
    cardID = 0

    def __init__(self, idnum, size, numberSet):
        """Card constructor"""
        self.myNumbers = []

        self.cardSize = size
        self.cardID = idnum

        i = 0
        while i < size*size:
            self.myNumbers.append(numberSet.getNext(size))
            i += 1

        return

    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.cardID

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.cardSize

    def rowSplitter(self, file=sys.stdout):
        i = 0
        print("+", file=file, end='')
        while i < self.cardSize:
            print("-----+", file=file, end='')
            i += 1
        print('', file=file)
        return

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        i = 0
        mid = self.cardSize/2
        while i < self.cardSize:
            print('', file=file)
            self.rowSplitter(file=file)
            j = 0
            if mid-1 < i < mid:
                print("|", file=file, end='')
                while j < self.cardSize:
                    if mid-1 < j < mid:
                        print("Free!|", file=file, end='')
                    else:
                        print(f'{self.myNumbers[i*self.cardSize+j]:5}|', file=file, end='')
                    j += 1
                i += 1
            else:
                print("|", file=file, end='')
                while j < self.cardSize:
                    print(f'{self.myNumbers[i*self.cardSize+j]:5}|', file=file, end='')
                    j += 1
                i += 1
        print('', file=file)
        self.rowSplitter(file=file)
        return
