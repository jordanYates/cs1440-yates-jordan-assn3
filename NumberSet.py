import random

class NumberSet():
    numberList = []
    getNextCounter = -1

    def __init__(self, size):
        """NumberSet constructor"""
        self.numberList = []
        i = 0
        while i < size:
            self.numberList.append(i + 1)
            i += 1
        return

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return len(self.numberList)

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if len(self.numberList) == 0:
            return None
        return self.numberList[index]

    def randomize(self):
        """void function: Shuffle this NumberSet"""
        random.shuffle(self.numberList)
        return

    def getNext(self, cardSize):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if len(self.numberList) == 0:
            return None
        if len(self.numberList) - 1 == self.getNextCounter:
            lastNum = self.numberList[self.getNextCounter]
            self.getNextCounter = 0
            return lastNum
        self.getNextCounter += 1
        return self.numberList[self.getNextCounter]