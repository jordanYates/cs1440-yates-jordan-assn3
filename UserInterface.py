import Deck
import Menu

class UserInterface():
    __m_currentDeck = None

    def __init__(self):
        pass

    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
            elif command == "X":
                keepGoing = False

    def __createDeck(self):
        """Command to create a new Deck"""
        cardSize = 0
        maxNumber = 0
        numberOfCards = 0

        keepGoing = True
        while keepGoing:
            print("What would you like your card size to be? (must be any number from 3 - 15)")
            cardSize = int(input())
            if 3 <= cardSize <= 15:
                keepGoing = False

        keepGoing = True
        while keepGoing:
            print("What would you like the maximum number to be?")
            maxNumber = int(input())
            if 2*cardSize*cardSize <= maxNumber <= 4*cardSize*cardSize:
                keepGoing = False

        keepGoing = True
        while keepGoing:
            print("How many cards would you like?")
            numberOfCards = int(input())
            if 1 <= numberOfCards <= 10000:
                keepGoing = False

        self.__m_currentDeck = Deck.Deck(cardSize, numberOfCards, maxNumber)

        self.__deckMenu()

        pass

    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen");
        menu.addOption("D", "Display the whole deck to the screen");
        menu.addOption("S", "Save the whole deck to a file");

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False

    def __printCard(self):
        """Command to print a single card"""
        cardToPrint = self.__getNumberInput("Id of card to print", 1, self.__m_currentDeck.getCardCount())
        if cardToPrint > 0:
            print()
            self.__m_currentDeck.print(idx=cardToPrint)


    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name")
        if fileName != "":
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")

    def __getNumberInput(self, prompt, default, max):
        cardID = default
        keepGoing = True
        while keepGoing:
            print(prompt)
            cardID = int(input())
            if 1 <= cardID <= max:
                keepGoing = False
        return cardID

    def __getStringInput(self, prompt):
        print(prompt)
        return input()
